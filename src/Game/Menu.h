//
// Created by Jan Mráz on 2019-04-25.
//

#ifndef RPG_SEMINAR_MENU_H
#define RPG_SEMINAR_MENU_H


#include <string>
#include <vector>
#include <ncurses.h>

/**
 * InvalidMenuItems exception which is thrown if Menu is initialized without options
 */
class InvalidMenuItems : std::exception {
};

/**
 * Menu is class responsible for creating menu options and handling which option user will choose
 */
class Menu {
    WINDOW *m_window;
    std::vector<std::string> m_items;
    int m_size;
    int m_position;
    int m_start_x;
    int m_start_y;


    /**
     * Print menu items into window each on new line
     */
    void printMenuItems();

public:
    Menu(WINDOW *window, const std::vector<std::string> &items, int start_x, int start_y);

    /**
     * Starts loop where user can choose option from menu items and after choosing it returns chosen index
     *
     * @return chosen option
     */
    int init();
};


#endif //RPG_SEMINAR_MENU_H
