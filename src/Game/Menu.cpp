//
// Created by Jan Mráz on 2019-04-25.
//

#include "Menu.h"
#include "Game.h"


Menu::Menu(WINDOW *window, const std::vector<std::string> &items, int start_y, int start_x) : m_window(window),
                                                                                              m_items(items),
                                                                                              m_size(items.size()),
                                                                                              m_position(0),
                                                                                              m_start_x(start_x),
                                                                                              m_start_y(start_y) {
    if (items.empty())
        throw InvalidMenuItems();
}

int Menu::init() {
    while (true) {
        printMenuItems();
        wrefresh(m_window);
        Game::drawBorders(m_window);

        int ch = wgetch(m_window);
        switch (ch) {
            case KEY_DOWN:
                m_position = ((m_position + 1) % m_size);
                break;
            case KEY_UP:
                m_position = ((m_position + m_size - 1) % m_size);
                break;
            case KEY_RIGHT:
                wclear(m_window);
                return m_position;
        }
        wrefresh(m_window);
    }
}

void Menu::printMenuItems() {
    for (int i = 0; i < m_size; i++) {
        mvwprintw(m_window, m_start_y + i, m_start_x, (i == m_position ? "[*] " : "[ ] "));
        wprintw(m_window, m_items[i].c_str());
    }
}
