//
// Created by Jan Mráz on 2019-04-05.
//

#include <iostream>
#include "Werewolf.h"

Werewolf::Werewolf(const std::string &name, int x, int y) : Monster(name, base_hp, base_armor, base_attack, 0, x, y) {

}

Werewolf::Werewolf(const std::string &name, int hp, int armor, int attack, int x, int y) : Monster(name, hp, armor,
                                                                                                   attack, 0, x, y) {

}

char Werewolf::getCharShortcut() const {
    return Werewolf::shortcut;
}