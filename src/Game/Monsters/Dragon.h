//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_DRAGON_H
#define RPG_SEMINAR_DRAGON_H


#include "Monster.h"

/**
  * Dragon is class representation of monster in the game. It inherits from {@link Monster}.
  * It has static constants as default stats.
  */
class Dragon : public Monster {
    static const int base_hp = 80;
    static const int base_armor = 3;
    static const int base_attack = 15;
public:
    static const char shortcut = 'D';

    Dragon(const std::string &name, int x, int y);

    Dragon(const std::string &name, int hp, int armor, int attack, int x, int y);

    /**
     * @inheritdoc
     */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_DRAGON_H
