//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_MONSTER_H
#define RPG_SEMINAR_MONSTER_H


#include "../Character.h"

/**
 * Exception thrown if there was an attempt to use mana on monster object
 */
class MonsterUsingMana : std::exception {

};

/**
 * Exception thrown if there was an attempt to move with Monster
 */
class InvalidMoveOfMonster : std::exception {

};

/**
  * Monster is base class to all monsters such as {@link Dragon},{@link Skeleton} and {@link Werewolf}.
  */
class Monster : public Character {
    std::string description;
public:
    Monster(const std::string &name, int hp, int armor, int attack, int mana, int x, int y);

    /**
      * @inheritdoc
      */
    int getMana() const override;

    /**
      * @inheritdoc
      */
    void useMana(int mana) override;

    /**
      * @inheritdoc
      */
    bool hasEnoughMana(int mana) const override;

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override = 0;

    /**
      * @inheritdoc
      */
    void setPositionX(int x) override;

    /**
      * @inheritdoc
      */
    void setPositionY(int y) override;
};


#endif //RPG_SEMINAR_MONSTER_H
