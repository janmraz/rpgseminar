//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_WEREWOLF_H
#define RPG_SEMINAR_WEREWOLF_H


#include "Monster.h"

/**
  * Werewolf is class representation of monster in the game. It inherits from {@link Monster}.
  * It has static constants as default stats.
  */
class Werewolf : public Monster {
    static const int base_hp = 60;
    static const int base_armor = 5;
    static const int base_attack = 10;
public:
    static const char shortcut = 'W';

    Werewolf(const std::string &name, int x, int y);

    Werewolf(const std::string &name, int armor, int hp, int attack, int x, int y);

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_WEREWOLF_H
