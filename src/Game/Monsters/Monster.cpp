//
// Created by Jan Mráz on 2019-04-05.
//

#include "Monster.h"

Monster::Monster(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Character(name, hp,
                                                                                                          armor, attack,
                                                                                                          mana, x, y) {

}

void Monster::useMana(int mana) {
    throw MonsterUsingMana();
}

bool Monster::hasEnoughMana(int mana) const {
    return false;
}

int Monster::getMana() const {
    return 0;
}

void Monster::setPositionX(int x) {
    throw InvalidMoveOfMonster();
}

void Monster::setPositionY(int y) {
    throw InvalidMoveOfMonster();
}
