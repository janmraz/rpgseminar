//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_SKELETON_H
#define RPG_SEMINAR_SKELETON_H


#include "Monster.h"

/**
  * Skeleton is class representation of monster in the game. It inherits from {@link Monster}.
  * It has static constants as default stats.
  */
class Skeleton : public Monster {
    static const int base_hp = 30;
    static const int base_armor = 7;
    static const int base_attack = 8;
public:
    static const char shortcut = 'S';

    Skeleton(const std::string &name, int x, int y);

    Skeleton(const std::string &name, int armor, int hp, int attack, int x, int y);

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_SKELETON_H
