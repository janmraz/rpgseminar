//
// Created by Jan Mráz on 2019-04-04.
//

#include "Item.h"

Item::Item(const std::string &name, const std::string &description, int x, int y)
        : Entity(x, y), m_name(name), m_description(description) {

}

void Item::loot() {
    m_looted = true;
}

bool Item::wasLooted() {
    return m_looted;
}

void Item::setPositionX(int) {
    throw InvalidMoveOfItem();
}

void Item::setPositionY(int) {
    throw InvalidMoveOfItem();
}