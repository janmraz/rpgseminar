//
// Created by Jan Mráz on 2019-04-04.
//

#ifndef RPG_SEMINAR_ITEM_H
#define RPG_SEMINAR_ITEM_H


#include <string>
#include "../Entity.h"

/**
 * Exception thrown if there was an attempt to move item's position.
 */
class InvalidMoveOfItem : std::exception {
};

/**
  * Item is abstract representation of items that can be found in the game. It is derived from {@link Entity}.
  * It is a base class for {@link Armor},{@link Weapon},{@link Elixir} and {@link Potion}.
  * It is responsible for storing name, description. Every item has certain amount that represents bonus,
  * for example {@link Weapon}, which adds attack power. So combination of getType and amount can be determined which bonus
  * should be added to {@link Hero}
  */
struct Item : public Entity {
    std::string m_name;
    std::string m_description;
    bool m_looted = false;

    Item(const std::string &name, const std::string &description, int x, int y);

    /**
     * Gets amount of bonus that item adds
     *
     * @return int amount of bonus
     */
    virtual int getAmount() const = 0;

    /**
     * @inheritdoc
     */
    char getCharShortcut() const override = 0;

    /**
     * Loots item, so it sets item's looted flag to true
     */
    void loot();

    /**
     * Checks whether item has been looted
     *
     * @return true if was already looted
     */
    bool wasLooted();

    /**
     * @inheritdoc
     */
    void setPositionX(int x) override;

    /**
     * @inheritdoc
     */
    void setPositionY(int y) override;
};


#endif //RPG_SEMINAR_ITEM_H
