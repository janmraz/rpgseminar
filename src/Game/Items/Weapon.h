//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_WEAPON_H
#define RPG_SEMINAR_WEAPON_H


#include "Item.h"

/**
  * Weapon represents item that can be found in the game, which adds attack power to {@link Hero}.
  * It holds static string representing his type (has to be unique amongst classes derived from Item).
  */
struct Weapon : public Item {
    int m_amount;
    static const char shortcut = 'W';

    Weapon(const std::string &name, const std::string &description, int plusDamage, int x, int y);

    /**
     * Gets amount of armor that should be added to {@link Hero}
     *
     * @return amount of armor
     */
    int getAmount() const override;

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_WEAPON_H
