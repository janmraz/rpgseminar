//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_POTION_H
#define RPG_SEMINAR_POTION_H


#include "Item.h"

/**
  * Potion represents item that can be found in the game, which adds HP to {@link Hero}
  * It holds static string representing his type (has to be unique amongst classes derived from Item).
  */
struct Potion : public Item {
    int m_amount;
    static const char shortcut = 'P';

    Potion(const std::string &name, const std::string &description, int amount, int x, int y);

    /**
     * Gets amount of HP that should be added to {@link Hero}
     *
     * @return amount of HP
     */
    int getAmount() const override;


    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_POTION_H
