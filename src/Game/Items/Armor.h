//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_REGULARITEM_H
#define RPG_SEMINAR_REGULARITEM_H

#include "Item.h"

/**
  * Armor represents item that can be found in the game, which adds armor to {@link Hero}
  * It holds static string representing his type (has to be unique amongst classes derived from Item).
  */
struct Armor : public Item {
    int m_amount;
    static const char shortcut = 'A';

    Armor(const std::string &name, const std::string &description, int amountDefence, int x, int y);

    /**
     * Gets amount of armor that should be added to {@link Hero}
     *
     * @return amount of armor
     */
    int getAmount() const override;

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_REGULARITEM_H
