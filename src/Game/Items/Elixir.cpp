//
// Created by Jan Mráz on 2019-04-15.
//

#include "Elixir.h"

Elixir::Elixir(const std::string &name, const std::string &description, int amount, int x, int y)
        : Item(name, description, x, y), m_amount(amount) {

}

char Elixir::getCharShortcut() const {
    return Elixir::shortcut;
}

int Elixir::getAmount() const {
    return m_amount;
}

