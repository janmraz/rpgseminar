//
// Created by Jan Mráz on 2019-04-15.
//

#ifndef RPG_SEMINAR_ELIXIR_H
#define RPG_SEMINAR_ELIXIR_H

#include "Item.h"

/**
  * Elixir represents item that can be found in the game, which adds mana to {@link Hero}
  * It holds static string representing his type (has to be unique amongst classes derived from Item).
  */
struct Elixir : public Item {
    int m_amount;
    static const char shortcut = 'E';

    Elixir(const std::string &name, const std::string &description, int amount, int x, int y);

    /**
     * Gets amount of mana that should be added to {@link Hero}
     *
     * @return amount of mana
     */
    int getAmount() const override;

    /**
    * @inheritdoc
    */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_ELIXIR_H
