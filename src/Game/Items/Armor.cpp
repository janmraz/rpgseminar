//
// Created by Jan Mráz on 2019-04-05.
//

#include "Armor.h"

Armor::Armor(const std::string &name, const std::string &description, int amountDefence, int x, int y)
        : Item(name, description, x, y), m_amount(amountDefence) {

}

char Armor::getCharShortcut() const {
    return Armor::shortcut;
}

int Armor::getAmount() const {
    return m_amount;
}
