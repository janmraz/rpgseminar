//
// Created by Jan Mráz on 2019-04-05.
//

#include "Weapon.h"

Weapon::Weapon(const std::string &name, const std::string &description, int plusDamage, int x, int y)
        : Item(name, description, x, y), m_amount(plusDamage) {

}

char Weapon::getCharShortcut() const {
    return Weapon::shortcut;
}

int Weapon::getAmount() const {
    return m_amount;
}
