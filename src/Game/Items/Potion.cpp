//
// Created by Jan Mráz on 2019-04-05.
//

#include "Potion.h"

Potion::Potion(const std::string &name, const std::string &description, int amount, int x, int y)
        : Item(name, description, x, y), m_amount(amount) {

}

char Potion::getCharShortcut() const {
    return Potion::shortcut;
}

int Potion::getAmount() const {
    return m_amount;
}
