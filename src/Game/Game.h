//
// Created by Jan Mráz on 2019-04-04.
//

#ifndef RPG_SEMINAR_GAME_H
#define RPG_SEMINAR_GAME_H


#include <string>
#include <ncurses.h>
#include "Map.h"
#include "Hero/Hero.h"
#include "FightEngine.h"

/**
 * Exception thrown if loading file has incorrect number of columns
 */
class LineHasTooManyColumns : std::exception {
};

/**
 * Exception thrown if loading file has incorrect char representation for monster
 */
class InvalidMonsterType : std::exception {
};

/**
 * Exception thrown if loading file has incorrect char representation for item
 */
class InvalidItemType : std::exception {
};

/**
 * Exception thrown if loading file has incorrect char representation for user
 */
class InvalidUserType : std::exception {
};

/**
 * Game is main object of the whole game. It takes care of main flow thru the game.
 * It manages all the windows and borders around them. It holds current {@link Hero} and {@link Map}.
 * It takes care of loading and saving the game.It holds database for all items, monsters in the game.
 * It checks total victory conditions. It handles player choosing and it initialize the fights.
 */
class Game {
    Map m_map;
    std::shared_ptr <Hero> m_player;
    std::vector <std::shared_ptr<Item>> m_items;
    std::vector <std::shared_ptr<Monster>> m_monsters;
    WINDOW *m_main;
    WINDOW *m_side;
    WINDOW *m_bottom;

    /**
     * Prints map into main window
     */
    void printMap();

    /**
     * Loads one item based on his type and add it into map or inventory
     *
     * @param file input stream
     * @param itemType type shortcut for item
     * @param addToInventory if true it adds item to inventory, otherwise into map
     */
    void loadItem(std::ifstream &file, char itemType, bool addToInventory);

    /**
     * Loads items into map
     *
     * @param path to file
     */
    void loadItems(const std::string &path);

    /**
     * Loads one monster based on his type and add it into map
     *
     * @param file input stream
     * @param monsterType type shortcut for monster
     */
    void loadMonster(std::ifstream &file, char monsterType);

    /**
     * Loads monsters into map
     *
     * @param path to file
     */
    void loadMonsters(const std::string &path);

    /**
     * Loads items into inventory
     *
     * @param file input stream
     */
    void loadInventoryItems(std::ifstream &file);

    /**
     * Loads user from provided path. It loads all users' stats and his entire inventory
     *
     * @param path to file
     */
    void loadUser(const std::string &path);

    /**
     * Checks whether file exists
     *
     * @param path to file
     * @return true if file can be loaded
     */
    bool checkFileExistence(const std::string &path);

    /**
     * Prompts user to choose a hero.
     */
    void playerChoosing();

    /**
     * Gets user input as string. It display header and returns text that user typed in.
     *
     * @param header text that should be displayed
     * @return user input as string
     */
    std::string getTextInput(const std::string &header);

    /**
     *  Displays map and allow user to move across it.
     */
    void control();

    /**
     * Process the fight between user and provided monster and
     * returns true if player has won or if he has been defeated
     *
     * @param monster to fight
     * @return true if victory or defeat has been achieved
     */
    bool fight(std::shared_ptr <Monster> &monster);

    /**
     * Refresh all the windows and redraws borders
     */
    void refreshScreen();

    /**
     * Checks whether winning conditions are met
     *
     * @return true if player has won
     */
    bool checkTotalVictory();

    /**
     * Prints player's victory on main window
     */
    void printVictory();

    /**
     * Prints player's defeat on main window
     */
    void printDefeat();

    /**
     * Saves current game into save files that are named based on user's input
     */
    void saveGame();

    /**
     * Loads game from saved file that user filled in
     */
    void loadGame();

    /**
     * Loads default map, player, monsters and items
     */
    void loadDefaultGame();

    /**
     * Displays main menu
     */
    void mainMenu();

    /**
     * Displays resumed menu
     */
    void resumeMenu();

public:
    /**
     * Draws border around the window
     *
     * @param window
     */
    static void drawBorders(WINDOW *window);

    /**
     * Refresh hero's stats in provided window
     *
     * @param window
     * @param player of which stats should be updated
     */
    static void refreshStats(WINDOW *window, const std::shared_ptr <Hero> &player);

    /**
     * Default constructor initiates the ncurses windows and sets configuration for ncurses
     */
    Game();

    /**
     * Starts the game
     */
    void start();
};


#endif //RPG_SEMINAR_GAME_H
