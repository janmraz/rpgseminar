//
// Created by Jan Mráz on 2019-04-04.
//

#ifndef RPG_SEMINAR_PLACE_H
#define RPG_SEMINAR_PLACE_H

#include <vector>
#include "Items/Item.h"
#include "Monsters/Monster.h"

/**
  * PLACE_TYPE visual type represention of one place in map
  */
enum PLACE_TYPE {
    Wall = '#',
    Empty = ' ',
    Chest = 'T',
    UserRight = '>',
    UserLeft = '<',
    UserUp = '^',
    UserDown = 'v',
};

/**
  * InvalidPlaceTypeRepresentation is exception which is thrown if invalid char representation of place
  */
class InvalidPlaceTypeRepresentation : std::exception {
};

/**
  * Place is structure that represents one place in map. It has type value representing visual representation on screen.
  * It can hold monster or vector of items(looting chest) or both(enemy's attack and then spoil).
  */
class Place {
    PLACE_TYPE m_type;
    std::vector<std::shared_ptr<Item>> m_items;
    std::shared_ptr<Monster> m_monster = nullptr;
public:

    /**
     * Get value of PLACE_TYPE enum
     *
     * @param input char representation
     * @throws InvalidPlaceTypeRepresentation exception when char is not valid
     * @return value of enum type PLACE_TYPE
     */
    static PLACE_TYPE determinePlaceType(char input);

    explicit Place(PLACE_TYPE place_type);

    /**
     * Update PLACE_TYPE enum value
     *
     * @param place_type
     */
    void updateType(PLACE_TYPE place_type);

    /**
     * Add pointer to Item into m_items and updates type to chest or empty (monster is here).
     * If monster is here then item is not part of chest but spoil of victory.
     *
     * @param item is a pointer to item
     */
    void addItem(const std::shared_ptr<Item> &item);

    /**
     * Gets Items that are stored in this place
     *
     * @return vector of pointers of items
     */
    std::vector<std::shared_ptr<Item>> &getItems();

    /**
     * Delete all Items stored in this place
     */
    void clearItems();

    /**
     * Add monster to this place. And update type to empty.
     *
     * @param monster shared_ptr of Monster to save
     */
    void setMonster(const std::shared_ptr<Monster> &monster);

    /**
     * Gets monster that is stored here (default value is nullptr)
     *
     * @return shared_ptr of Monster stored in this place
     */
    std::shared_ptr<Monster> &getMonster();

    /**
     * Remove monster stored in this place
     */
    void removeMonster();

    /**
     * Checks if there are some items so this place is chest type
     *
     * @return true, if this place is chest
     */
    bool isChest() const;

    /**
     * Checks if this place is Wall
     *
     * @return true, if this place is wall
     */
    bool isWall() const;

    /**
     * Checks if this place has monster
     *
     * @return true, if this place has monster
     */
    bool isMonster() const;

    /**
     * Return enum value of type of this place
     *
     * @return value of place type enum
     */
    PLACE_TYPE getType() const;

    /**
     * Gets char representation of type of this place
     *
     * @return char representation of this place type
     */
    char getChar() const;
};


#endif //RPG_SEMINAR_PLACE_H
