//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_FIGHT_H
#define RPG_SEMINAR_FIGHT_H


#include <ncurses.h>
#include "Monsters/Monster.h"
#include "Hero/Hero.h"

/**
 * FightEngine is class responsible for fight amongst {@link Hero} and {@link Monster}.
 * In constructor it gets window into which is whole move-based fight printed.
 * It take care of menu of choosing spells. Printing intro and victory and defeat status.
 * And it is main and only class where is fight system of whole game is defined.
 *
 * Fighting system consists of three parts:
 * 1. Determine attack power (getting armor and base attacks)
 * 2. Chances calculation
 *     - Calculation of lucky bonus (add extra power to your attack)
 *     - Calculation of critical bonus (if spell has this type)
 * 3. Dealing the damage
 */
class FightEngine {
private:
    std::shared_ptr<Monster> m_monster;
    std::shared_ptr<Hero> m_hero;
    WINDOW *m_main;
    WINDOW *m_side;

    /**
     * Counts lucky attack bonus from base attack based on randomized chance of dealing.
     * If character has no luck, it returns 0.
     * Current chance is 1:60
     *
     * @param attackPower base attack
     * @return lucky bonus
     */
    int getLuckyBonus(int attackPower);

    /**
     * Counts Critical attack bonus from base attack based on randomized chance of dealing.
     * If character has no luck, it returns 0.
     * Current chance is 1:5
     *
     * @param attackPower base attack
     * @return critical bonus
     */
    int getCriticalAttack(int attackPower);

    /**
     * Counts the attack power based on armor and attack power, lucky bonus and critical damage.
     *
     * @param attacking character
     * @param defending character
     * @return power of attack that attacking should deal to defending
     */
    int determineAttack(const Character &attacking, const Character &defending);

    /**
     * Prints intro screen when hero enters fight.
     */
    void printIntro() const;

    /**
     * Prints screen describing that user has not enough spell for attack
     */
    void printNotEnoughMana(int requiredMana) const;

    /**
     * Prints victory screen when hero has won the fight.
     */
    void printVictory() const;

    /**
     * Prints defeat screen when hero has been defeated by the monster.
     */
    void printDefeat() const;

    /**
     * Prints screen describing how user's attack went and how much damage he dealt
     *
     * @param attack of user
     */
    void printHeroAttackImpact(int attack) const;

    /**
     * Prints screen describing how monsters's attack went and how much damage he dealt
     *
     * @param attack of monster
     */
    void printMonsterAttackImpact(int attack) const;

    /**
     * Checks whether hero or monster has died
     *
     * @return true if someone is dead
     */
    bool checkEndOfFight() const;

    /**
     * Makes an hero's regular attack. Determines its attack power and deals the damage.
     *
     * @return
     */
    bool HeroMove();

    /**
     * Makes an monster's attack. Determines its attack power and deals the damage.
     *
     * @return
     */
    bool MonsterMove();

    /**
     * Makes an hero's spell attack. Determines its attack power and deals the damage.
     *
     * @return
     */
    bool HeroMove(const Spell &spell);

public:
    FightEngine(std::shared_ptr<Monster> &monster, std::shared_ptr<Hero> &hero, WINDOW *window, WINDOW *statusBar);

    /**
     * Starts the whole fight. It takes over screen for entire fight
     *
     * @return true if hero has won
     */
    bool start();
};


#endif //RPG_SEMINAR_FIGHT_H
