//
// Created by Jan Mráz on 2019-05-08.
//

#ifndef RPG_SEMINAR_CONFIG_H
#define RPG_SEMINAR_CONFIG_H

/**
 * Config is static class which holds general configuration.
 */
struct Config {
    static const int MapXIndentation = 5;
    static const int MapYIndentation = 10;
    static const int TextIndentation = 5;
    static const int TextLimit = 30;
};


#endif //RPG_SEMINAR_CONFIG_H
