//
// Created by Jan Mráz on 2019-05-08.
//

#ifndef RPG_SEMINAR_ENTITY_H
#define RPG_SEMINAR_ENTITY_H

/**
 * Entity is base class for all items/heroes/monsters that are displayed on screen so Entity need to store position in map.
 * Then all these entities has to be saved so they all need short identification so every subclass has to implement
 * method which is returning char identification of that entity.
 */
class Entity {
protected:
    int m_position_x;
    int m_position_y;
public:
    /**
     * Gets x position of an entity in map
     *
     * @return int representing x position
     */
    int getPositionX() const;

    /**
      * Gets y position of an entity in map
      *
      * @return int representing y position
      */
    int getPositionY() const;

    /**
     * Returns char shortcut used during loading and saving game
     *
     * @return char representation
     */
    virtual char getCharShortcut() const = 0;

    /**
     * Set position x of an entity
     */
    virtual void setPositionX(int x) = 0;

    /**
     * Set position y of an entity
     */
    virtual void setPositionY(int y) = 0;

public:
    Entity(int x, int y);
    virtual ~Entity() = default;
};


#endif //RPG_SEMINAR_ENTITY_H
