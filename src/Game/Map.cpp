//
// Created by Jan Mráz on 2019-04-04.
//

#include <iostream>
#include <ncurses.h>
#include <fstream>
#include "Map.h"
#include "Config.h"

Map::Map(const std::string &path, std::shared_ptr<Hero> &player) : m_player(player) {
    m_height = 0;
    m_width = 0;

    std::ifstream file(path);
    std::vector<std::vector<Place>> finalMap;
    std::string line;

    while (std::getline(file, line)) {
        m_height++;
        std::vector<Place> lineVector;
        for (char i : line) {
            PLACE_TYPE type = Place::determinePlaceType(i);
            if (type != PLACE_TYPE::Empty && type != PLACE_TYPE::Wall)
                type = PLACE_TYPE::Empty;
            lineVector.emplace_back(type);
            m_width++;
        }
        m_map.emplace_back(lineVector);
    }

    if (!checkWithinBoundaries(m_player->getPositionX(), m_player->getPositionY()))
        throw OutOfMapBoundaries();
    if (m_map[m_player->getPositionX()][m_player->getPositionY()].isWall())
        throw PlayerPlacedInWall();

    updatePlayer();
}

void Map::exportMap(std::ofstream & ofstream) const {
    for(const auto& line : m_map){
        for(const auto& place : line){
            char type = place.getChar();
            if (type != PLACE_TYPE::Empty && type != PLACE_TYPE::Wall)
                type = PLACE_TYPE::Empty;
            ofstream << type;
        }
        ofstream << std::endl;
    }
}


bool Map::checkWithinBoundaries(int x, int y) const {
    return (x >= 0 && x < m_height && y >= 0 && y < m_width);
}

void Map::printMap(WINDOW *w) const {
    int i = Config::MapXIndentation;
    for (const auto &line : m_map) {
        int j = Config::MapYIndentation;
        for (const auto &position: line) {
            char c = position.getChar();
            mvwprintw(w, i, j, "%c", c);
            j++;
        }
        i++;
    }
}

bool Map::goAhead() {
    Place ahead = getAheadPlace();
    int x = m_player->getPositionX();
    int y = m_player->getPositionY();

    if (ahead.isWall()) {
        return false;
    }

    for (const auto &item : ahead.getItems())
        m_player->addItem(item);
    m_map[x][y].clearItems();

    m_map[x][y].updateType(Empty);
    switch (m_orientation) {
        case UserUp:
            m_map[x--][y];
            break;
        case UserRight:
            m_map[x][y++];
            break;
        case UserDown:
            m_map[x++][y];
            break;
        case UserLeft:
            m_map[x][y--];
            break;
        default:
            throw InvalidOrientation();
    }
    m_map[x][y].updateType(m_orientation);
    m_player->setPositionX(x);
    m_player->setPositionY(y);
    return true;
}

void Map::rotatePlayer(bool clockWise) {
    PLACE_TYPE tmp;
    switch (m_orientation) {
        case UserUp:
            tmp = clockWise ? UserRight : UserLeft;
            break;
        case UserRight:
            tmp = clockWise ? UserDown : UserUp;
            break;
        case UserDown:
            tmp = clockWise ? UserLeft : UserRight;
            break;
        case UserLeft:
            tmp = clockWise ? UserUp : UserDown;
            break;
        default:
            throw InvalidOrientation();
    }
    m_orientation = tmp;
    updatePlayer();
}

void Map::updatePlayer() {
    m_map[m_player->getPositionX()][m_player->getPositionY()].updateType(m_orientation);
}

Place &Map::getAheadPlace() {
    switch (m_orientation) {
        case UserUp:
            return m_map[m_player->getPositionX() - 1][m_player->getPositionY()];
        case UserRight:
            return m_map[m_player->getPositionX()][m_player->getPositionY() + 1];
        case UserDown:
            return m_map[m_player->getPositionX() + 1][m_player->getPositionY()];
        case UserLeft:
            return m_map[m_player->getPositionX()][m_player->getPositionY() - 1];
        default:
            throw InvalidOrientation();
    }
}

void Map::addItem(const std::shared_ptr<Item> &item, int x, int y) {
    if (!checkWithinBoundaries(x, y))
        throw OutOfMapBoundaries();
    if (m_map[x][y].isWall())
        throw AddingItemIntoWall();

    std::cout << "adding item " << item->m_name << " into map" << std::endl;
    m_map[x][y].addItem(item);
}

std::shared_ptr<Monster> &Map::getEnemy() {
    if(!m_map[m_player->getPositionX()][m_player->getPositionY()].isMonster())
        throw NoMonsterInPlace();
    return m_map[m_player->getPositionX()][m_player->getPositionY()].getMonster();
}

bool Map::isEnemy() const {
    return m_map[m_player->getPositionX()][m_player->getPositionY()].isMonster();
}

void Map::removeEnemy() {
    m_map[m_player->getPositionX()][m_player->getPositionY()].removeMonster();
}

void Map::addMonster(const std::shared_ptr<Monster> &monster, int x, int y) {
    if (!checkWithinBoundaries(x, y))
        throw OutOfMapBoundaries();

    if (m_map[x][y].isWall())
        throw AddingMonsterIntoWall();
    if (m_map[x][y].isMonster())
        throw MoreMonstersInOnePlace();

    std::cout << "adding monster " << monster->getName() << " into map" << std::endl;
    m_map[x][y].setMonster(monster);
}

Map::Map() {

}
