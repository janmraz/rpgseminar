//
// Created by Jan Mráz on 2019-05-08.
//

#include "Entity.h"

Entity::Entity(int x, int y): m_position_x(x), m_position_y(y) {

}

int Entity::getPositionY() const {
    return m_position_y;
}

int Entity::getPositionX() const {
    return m_position_x;
}