//
// Created by Jan Mráz on 2019-04-12.
//

#include "Spell.h"

Spell::Spell(int mana, int attack, SPECIAL_TYPE type) : m_mana(mana), m_attack(attack), m_type(type) {
    if(mana < 0 || attack < 0 || type < 0)
        throw InvalidSpellStats();
}

