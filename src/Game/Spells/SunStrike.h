//
// Created by Jan Mráz on 2019-04-19.
//

#ifndef RPG_SEMINAR_SUNSTRIKE_H
#define RPG_SEMINAR_SUNSTRIKE_H


#include "Spell.h"

/**
 * SunStrike is struct representing spell that can {@link Hero} cast on its enemies during fight.
 * It is derived from base struct {@link Spell}.
 * Its stats are stored in static constants in this struct.
 */
struct SunStrike : public Spell {
    static const int mana = 10;
    static const int attack = 90;
    static const SPECIAL_TYPE type = Empty;
    static const std::string name;

    SunStrike();

    /**
      * @inheritdoc
      */
    const std::string &getName() const override;
};


#endif //RPG_SEMINAR_SUNSTRIKE_H
