//
// Created by Jan Mráz on 2019-04-19.
//

#include "FatalBlow.h"

const std::string FatalBlow::name = "Fatal Blow";

FatalBlow::FatalBlow() : Spell(FatalBlow::mana, FatalBlow::attack, FatalBlow::type) {

}

const std::string &FatalBlow::getName() const {
    return FatalBlow::name;
}
