//
// Created by Jan Mráz on 2019-04-19.
//

#include "SinisterStab.h"

const std::string SinisterStab::name = "Sinister Stab";

SinisterStab::SinisterStab() : Spell(SinisterStab::mana, SinisterStab::attack, SinisterStab::type) {

}

const std::string &SinisterStab::getName() const {
    return SinisterStab::name;
}
