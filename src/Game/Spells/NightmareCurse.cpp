//
// Created by Jan Mráz on 2019-04-19.
//

#include "NightmareCurse.h"

const std::string NightmareCurse::name = "Nightmare Curse";

NightmareCurse::NightmareCurse() : Spell(NightmareCurse::mana, NightmareCurse::attack, NightmareCurse::type) {

}

const std::string &NightmareCurse::getName() const {
    return NightmareCurse::name;
}
