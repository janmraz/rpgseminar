//
// Created by Jan Mráz on 2019-04-19.
//

#include "RottingSmash.h"

const std::string RottingSmash::name = "Rotting Smash";

RottingSmash::RottingSmash() : Spell(RottingSmash::mana, RottingSmash::attack, RottingSmash::type) {

}

const std::string &RottingSmash::getName() const {
    return RottingSmash::name;
}
