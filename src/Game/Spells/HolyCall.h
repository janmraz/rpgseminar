//
// Created by Jan Mráz on 2019-04-18.
//

#ifndef RPG_SEMINAR_HOLYCALL_H
#define RPG_SEMINAR_HOLYCALL_H


#include "Spell.h"

/**
 * HolyCall is struct representing spell that can {@link Hero} cast on its enemies during fight.
 * It is derived from base struct {@link Spell}.
 * Its stats are stored in static constants in this struct.
 */
struct HolyCall : public Spell {
    static const int mana = 40;
    static const int attack = 200;
    static const SPECIAL_TYPE type = Empty;
    static const std::string name;

    HolyCall();

    /**
      * @inheritdoc
      */
    const std::string &getName() const override;
};


#endif //RPG_SEMINAR_HOLYCALL_H
