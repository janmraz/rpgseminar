//
// Created by Jan Mráz on 2019-04-19.
//

#include "SunStrike.h"

const std::string SunStrike::name = "Sun Strike";

SunStrike::SunStrike() : Spell(SunStrike::mana, SunStrike::attack, SunStrike::type) {

}

const std::string &SunStrike::getName() const {
    return SunStrike::name;
}
