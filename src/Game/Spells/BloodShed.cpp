//
// Created by Jan Mráz on 2019-04-19.
//

#include "BloodShed.h"

const std::string BloodShed::name = "Blood Shed";

BloodShed::BloodShed() : Spell(BloodShed::mana, BloodShed::attack, BloodShed::type) {

}

const std::string &BloodShed::getName() const {
    return BloodShed::name;
}
