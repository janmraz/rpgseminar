//
// Created by Jan Mráz on 2019-04-19.
//

#ifndef RPG_SEMINAR_SINISTERSTAB_H
#define RPG_SEMINAR_SINISTERSTAB_H


#include "Spell.h"

/**
 * SinisterStab is struct representing spell that can {@link Hero} cast on its enemies during fight.
 * It is derived from base struct {@link Spell}.
 * Its stats are stored in static constants in this struct.
 */
struct SinisterStab : public Spell {
    static const int mana = 30;
    static const int attack = 150;
    static const SPECIAL_TYPE type = CriticalAttack;
    static const std::string name;

    SinisterStab();

    /**
      * @inheritdoc
      */
    const std::string &getName() const override;
};


#endif //RPG_SEMINAR_SINISTERSTAB_H
