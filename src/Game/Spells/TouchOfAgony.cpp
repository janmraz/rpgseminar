//
// Created by Jan Mráz on 2019-04-19.
//

#include "TouchOfAgony.h"

const std::string TouchOfAgony::name = "Touch of Agony";

TouchOfAgony::TouchOfAgony() : Spell(TouchOfAgony::mana, TouchOfAgony::attack, TouchOfAgony::type) {

}

const std::string &TouchOfAgony::getName() const {
    return TouchOfAgony::name;
}
