//
// Created by Jan Mráz on 2019-04-12.
//

#ifndef RPG_SEMINAR_SPELL_H
#define RPG_SEMINAR_SPELL_H


#include <string>

/**
 * Exception thrown if there was an attempt to initialize Spell with invalid(negative) values
 */
class InvalidSpellStats : std::exception {
};

/**
 * Spell abstract struct is object holding spell features such SPECIAL_TYPE, amount of mana and amount attack power.
 * It is used as base struct for all spells.
 */
struct Spell {
    /**
     * Enum representing special effect that spell can have
     */
    enum SPECIAL_TYPE {
        CriticalAttack,
        IgnoreArmor,
        Empty
    };

    int m_mana;
    int m_attack;
    SPECIAL_TYPE m_type;

    Spell(int mana, int attack, SPECIAL_TYPE type);

    virtual ~Spell() = default;

    /**
     * Gets Name of spell
     *
     * @return string representing name of attack
     */
    virtual const std::string &getName() const = 0;
};


#endif //RPG_SEMINAR_SPELL_H
