//
// Created by Jan Mráz on 2019-04-19.
//

#ifndef RPG_SEMINAR_TOUCHOFAGONY_H
#define RPG_SEMINAR_TOUCHOFAGONY_H


#include "Spell.h"

/**
 * TouchOfAgony is struct representing spell that can {@link Hero} cast on its enemies during fight.
 * It is derived from base struct {@link Spell}.
 * Its stats are stored in static constants in this struct.
 */
struct TouchOfAgony : public Spell {
    static const int mana = 100;
    static const int attack = 300;
    static const SPECIAL_TYPE type = Empty;
    static const std::string name;

    TouchOfAgony();

    /**
      * @inheritdoc
      */
    const std::string &getName() const override;
};


#endif //RPG_SEMINAR_TOUCHOFAGONY_H
