//
// Created by Jan Mráz on 2019-04-19.
//

#include "HatefulRage.h"

const std::string HatefulRage::name = "Hateful Rage";

HatefulRage::HatefulRage() : Spell(HatefulRage::mana, HatefulRage::attack, HatefulRage::type) {

}

const std::string &HatefulRage::getName() const {
    return HatefulRage::name;
}
