//
// Created by Jan Mráz on 2019-04-18.
//

#include "HolyCall.h"

const std::string HolyCall::name = "Holy Call";

HolyCall::HolyCall() : Spell(HolyCall::mana, HolyCall::attack, HolyCall::type) {

}

const std::string &HolyCall::getName() const {
    return HolyCall::name;
}
