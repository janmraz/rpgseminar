//
// Created by Jan Mráz on 2019-04-04.
//

#include <iostream>
#include "Place.h"

PLACE_TYPE Place::determinePlaceType(char i) {
    switch (i) {
        case '#':
            return Wall;
        case ' ':
            return Empty;
        case 'T':
            return Chest;
        case '<':
            return UserLeft;
        case '>':
            return UserRight;
        case 'V':
            return UserDown;
        case '^':
            return UserUp;
        default:
            throw InvalidPlaceTypeRepresentation();
    }
}

Place::Place(PLACE_TYPE place_type) {
    m_type = place_type;
}

bool Place::isChest() const {
    return m_type == Chest;
}

bool Place::isWall() const {
    return m_type == Wall;
}

bool Place::isMonster() const {
    return m_monster != nullptr;
}

PLACE_TYPE Place::getType() const {
    return m_type;
}

void Place::updateType(PLACE_TYPE place_type) {
    m_type = place_type;
}

void Place::addItem(const std::shared_ptr<Item> &item) {
    m_items.push_back(item);

    // if there is monster then item is not a chest but victory's spoil
    if (isMonster())
        updateType(Empty);
    else
        updateType(Chest);
}

void Place::setMonster(const std::shared_ptr<Monster> &monster) {
    m_monster = monster;
    updateType(Empty);
}

void Place::clearItems() {
    m_items.clear();
    updateType(Empty);
}

std::vector<std::shared_ptr<Item>> &Place::getItems() {
    return m_items;
}

char Place::getChar() const {
    return (char) m_type;
}

std::shared_ptr<Monster> &Place::getMonster() {
    return m_monster;
}

void Place::removeMonster() {
    m_monster = nullptr;
}


