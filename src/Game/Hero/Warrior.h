//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_WARRIOR_H
#define RPG_SEMINAR_WARRIOR_H


#include "Hero.h"

/**
  * Warrior is class representation of playable hero in the game.
  * It holds static string representing his type (has to be unique amongst classes derived from Hero).
  * It has static constants as default stats.
  */
class Warrior : public Hero {
    static const int base_mana = 80;
    static const int base_armor = 20;
    static const int base_hp = 300;
    static const int base_attack = 90;
public:
    static const char shortcut = 'W';

    Warrior(const std::string &name, int x, int y);

    Warrior(const std::string &name, int hp, int armor, int attack, int mana, int x, int y);

    /**
     * @inheritdoc
     */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_WARRIOR_H
