//
// Created by Jan Mráz on 2019-04-05.
//

#include <iostream>
#include "Warrior.h"
#include "../Spells/HatefulRage.h"
#include "../Spells/RottingSmash.h"
#include "../Spells/SinisterStab.h"

Warrior::Warrior(const std::string &name, int x, int y) : Hero(name, base_hp, base_armor, base_attack, base_mana, x,
                                                               y) {
    m_spells.push_back(std::make_shared<HatefulRage>());
    m_spells.push_back(std::make_shared<RottingSmash>());
    m_spells.push_back(std::make_shared<SinisterStab>());
}

Warrior::Warrior(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Hero(name, hp, armor,
                                                                                                        attack,
                                                                                                        mana, x, y) {
    m_spells.push_back(std::make_shared<HatefulRage>());
    m_spells.push_back(std::make_shared<RottingSmash>());
    m_spells.push_back(std::make_shared<SinisterStab>());
}

char Warrior::getCharShortcut() const {
    return Warrior::shortcut;
}