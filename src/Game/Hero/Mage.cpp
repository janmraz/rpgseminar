//
// Created by Jan Mráz on 2019-04-05.
//

#include <iostream>
#include "Mage.h"
#include "../Spells/HolyCall.h"
#include "../Spells/SunStrike.h"
#include "../Spells/NightmareCurse.h"

Mage::Mage(const std::string &name, int x, int y) : Hero(name, base_hp, base_armor, base_attack, base_mana, x, y) {
    m_spells.push_back(std::make_shared<HolyCall>());
    m_spells.push_back(std::make_shared<NightmareCurse>());
    m_spells.push_back(std::make_shared<SunStrike>());
}

Mage::Mage(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Hero(name, hp, armor,
                                                                                                  attack, mana, x, y) {
    m_spells.push_back(std::make_shared<HolyCall>());
    m_spells.push_back(std::make_shared<NightmareCurse>());
    m_spells.push_back(std::make_shared<SunStrike>());
}

char Mage::getCharShortcut() const {
    return Mage::shortcut;
}