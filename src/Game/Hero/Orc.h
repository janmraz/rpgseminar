//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_ORC_H
#define RPG_SEMINAR_ORC_H


#include "Hero.h"

/**
  * Orc is class representation of playable hero in the game.
  * It holds static string representing his type (has to be unique amongst classes derived from Hero).
  * It has static constants as default stats.
  */
class Orc : public Hero {
    static const int base_mana = 200;
    static const int base_armor = 10;
    static const int base_hp = 500;
    static const int base_attack = 70;

public:
    static const char shortcut = 'O';

    Orc(const std::string &name, int x, int y);

    Orc(const std::string &name, int hp, int armor, int attack, int mana, int x, int y);

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override;
};


#endif //RPG_SEMINAR_ORC_H
