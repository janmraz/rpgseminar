//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_HERO_H
#define RPG_SEMINAR_HERO_H


#include <string>
#include "../Character.h"

/**
  * Hero is base class to all playable heroes such as {@link Orc},{@link Warrior} and {@link Mage}.
  * It has to implement mana management which is inherited from its base class {@link Character}
  * and position management from its root base class {@link Entity}
  */
class Hero : public Character {
protected:
    Hero(const std::string &name, int hp, int armor, int attack, int mana, int x, int y);

public:
    /**
      * @inheritdoc
      */
    int getMana() const override;

    /**
      * @inheritdoc
      */
    void useMana(int mana) override;

    /**
      * @inheritdoc
      */
    bool hasEnoughMana(int mana) const override;

    /**
      * @inheritdoc
      */
    char getCharShortcut() const override = 0;

    /**
      * @inheritdoc
      */
    void setPositionX(int x) override;

    /**
     * @inheritdoc
     */
    void setPositionY(int y) override;
};


#endif //RPG_SEMINAR_HERO_H
