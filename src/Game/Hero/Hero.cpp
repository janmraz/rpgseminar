//
// Created by Jan Mráz on 2019-04-05.
//

#include "Hero.h"

Hero::Hero(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Character(name, hp, armor,
                                                                                                       attack, mana, x,
                                                                                                       y) {

}

void Hero::useMana(int mana) {
    m_mana -= mana;
}

bool Hero::hasEnoughMana(int mana) const {
    return m_mana > mana;
}

int Hero::getMana() const {
    return m_mana;
}

void Hero::setPositionX(int x) {
    m_position_x = x;
}

void Hero::setPositionY(int y) {
    m_position_y = y;
}
