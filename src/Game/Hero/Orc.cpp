//
// Created by Jan Mráz on 2019-04-05.
//

#include <iostream>
#include "Orc.h"
#include "../Spells/FatalBlow.h"
#include "../Spells/BloodShed.h"
#include "../Spells/TouchOfAgony.h"

Orc::Orc(const std::string &name, int x, int y) : Hero(name, base_hp, base_armor, base_attack, base_mana, x, y) {
    m_spells.push_back(std::make_shared<FatalBlow>());
    m_spells.push_back(std::make_shared<BloodShed>());
    m_spells.push_back(std::make_shared<TouchOfAgony>());
}

Orc::Orc(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Hero(name, hp, armor, attack,
                                                                                                mana, x, y) {
    m_spells.push_back(std::make_shared<FatalBlow>());
    m_spells.push_back(std::make_shared<BloodShed>());
    m_spells.push_back(std::make_shared<TouchOfAgony>());
}

char Orc::getCharShortcut() const {
    return Orc::shortcut;
}
