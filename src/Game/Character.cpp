//
// Created by Jan Mráz on 2019-04-05.
//

#include "Character.h"
#include "Items/Potion.h"
#include "Items/Armor.h"
#include "Items/Weapon.h"
#include "Items/Elixir.h"

bool Character::isAlive() const {
    return m_hp > 0;
}

Character::Character(const std::string &name, int hp, int armor, int attack, int mana, int x, int y) : Entity(x, y),
                                                                                                       m_name(name),
                                                                                                       m_hp(hp),
                                                                                                       m_armor(armor),
                                                                                                       m_attack(attack),
                                                                                                       m_mana(mana) {
    if(hp < 0 || m_armor < 0 || m_attack < 0 || m_mana < 0)
        throw InvalidCharacterStats();
}

const std::string &Character::getName() const {
    return m_name;
}

const std::vector<std::shared_ptr<Spell>> &Character::getSpells() const {
    return m_spells;
}

void Character::getHit(int hp) {
    m_hp -= hp;
}

int Character::getHP() const {
    return m_hp;
}

void Character::addItem(const std::shared_ptr<Item> &item) {
    // during loading game
    if(item->wasLooted()){
        m_inventory.push_back(item);
        return;
    }
    switch ((*item).getCharShortcut()) {
        case Armor::shortcut:
            m_armor += (*item).getAmount();
            break;
        case Weapon::shortcut:
            m_attack += (*item).getAmount();
            break;
        case Potion::shortcut:
            m_hp += (*item).getAmount();
            break;
        case Elixir::shortcut:
            m_mana += (*item).getAmount();
            break;
    }
    (*item).loot();
    m_inventory.push_back(item);
}

std::vector<std::shared_ptr<Item>> &Character::getItems() {
    return m_inventory;
}

int Character::getAttack() const {
    return m_attack;
}

int Character::getArmor() const {
    return m_armor;
}

