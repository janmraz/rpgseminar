//
// Created by Jan Mráz on 2019-04-05.
//

#include "FightEngine.h"
#include "Game.h"
#include "Menu.h"
#include "Config.h"

void FightEngine::printIntro() const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You have been attacked by ");
    wprintw(m_main, m_monster->getName().c_str());
    mvwprintw(m_main, 7, Config::TextIndentation, "Enemy\'s HP: %d", m_monster->getHP());
    mvwprintw(m_main, 8, Config::TextIndentation, "Enemy\'s attack: %d", m_monster->getAttack());
    mvwprintw(m_main, 9, Config::TextIndentation, "Enemy\'s armor: %d", m_monster->getArmor());
    mvwprintw(m_main, 12, Config::TextIndentation, "< Press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

void FightEngine::printNotEnoughMana(int requiredMana) const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You no mana for this attack");
    mvwprintw(m_main, 5, Config::TextIndentation, "For this spell you need %d", requiredMana);
    mvwprintw(m_main, 6, Config::TextIndentation, "But you get only %d", m_hero->getMana());
    mvwprintw(m_main, 12, Config::TextIndentation, "< Press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

void FightEngine::printMonsterAttackImpact(int attack) const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You have been weakened by enemy attack. Your current hp is ");
    wprintw(m_main, "%d", m_hero->getHP());
    mvwprintw(m_main, 6, Config::TextIndentation, "Your attack has cause damage of power ");
    wprintw(m_main, "%d", attack);
    mvwprintw(m_main, 10, Config::TextIndentation, "< Press -> to continue >");

    Game::refreshStats(m_side, m_hero);

    while (wgetch(m_main) != KEY_RIGHT);
}

void FightEngine::printHeroAttackImpact(int attack) const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You have attacked on your enemy. ");
    mvwprintw(m_main, 6, Config::TextIndentation, "Your attack has cause damage of power ");
    wprintw(m_main, "%d", attack);
    mvwprintw(m_main, 7, Config::TextIndentation, "Enemy\'s HP: %d",
              m_monster->getHP() > 0 ? m_monster->getHP() : 0);
    mvwprintw(m_main, 8, Config::TextIndentation, "Enemy\'s attack: %d", m_monster->getAttack());
    mvwprintw(m_main, 9, Config::TextIndentation, "Enemy\'s armor: %d", m_monster->getArmor());
    mvwprintw(m_main, 12, Config::TextIndentation, "< Press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

void FightEngine::printVictory() const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You won. You beated the monster called ");
    wprintw(m_main, m_monster->getName().c_str());
    mvwprintw(m_main, 8, Config::TextIndentation, "< Press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

void FightEngine::printDefeat() const {
    wclear(m_main);
    Game::drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You have lost");
    wprintw(m_main, m_monster->getName().c_str());
    mvwprintw(m_main, 6, Config::TextIndentation, "Game Over");
    mvwprintw(m_main, 8, Config::TextIndentation, "< Press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

FightEngine::FightEngine(std::shared_ptr <Monster> &monster, std::shared_ptr <Hero> &hero, WINDOW *w, WINDOW *statusBar)
        : m_monster(monster), m_hero(hero), m_main(w), m_side(statusBar) {

}

bool FightEngine::HeroMove() {
    int baseAttack = determineAttack(*m_hero, *m_monster);

    baseAttack += getLuckyBonus(baseAttack);

    m_monster->getHit(baseAttack);
    printHeroAttackImpact(baseAttack);
    return true;
}

bool FightEngine::HeroMove(const Spell &spell) {
    if (!m_hero->hasEnoughMana(spell.m_mana)) {
        return false;
    }
    m_hero->useMana(spell.m_mana);

    int baseAttack = spell.m_attack;
    int baseArmor = m_monster->getArmor();

    if (spell.m_type == Spell::IgnoreArmor)
        baseArmor = 0;
    if (spell.m_type == Spell::CriticalAttack)
        baseAttack += getCriticalAttack(m_hero->getAttack());

    if (baseArmor > baseAttack)
        return false;

    m_monster->getHit(baseAttack - baseArmor);
    printHeroAttackImpact(baseAttack);
    return false;
}

bool FightEngine::MonsterMove() {
    int baseAttack = determineAttack(*m_monster, *m_hero);

    baseAttack += getLuckyBonus(baseAttack);

    m_hero->getHit(baseAttack);
    printMonsterAttackImpact(baseAttack);
    return false;
}

int FightEngine::determineAttack(const Character &attacking, const Character &defending) {
    int attackPower = attacking.getAttack();
    int armorPower = defending.getArmor();

    if (armorPower >= attackPower)
        return 0;
    int baseAttack = attackPower - armorPower;

    return baseAttack;
}

int FightEngine::getCriticalAttack(int attackPower) {
    if (attackPower == 0 || (rand() % 5) + 1 != 1)
        return 0;
    return (rand() % (attackPower / 2)) + 1;
}

int FightEngine::getLuckyBonus(int attackPower) {
    if (attackPower == 0 || (rand() % 60) + 1 != 1)
        return 0;
    return (rand() % (attackPower / 2)) + 1;
}

bool FightEngine::start() {
    printIntro();

    while (true) {
        wclear(m_main);

        mvwprintw(m_main, 2, Config::TextIndentation, "Choose your attack:");
        std::vector <std::string> items;
        items.emplace_back("Normal Attack");
        for (const auto &spell : m_hero->getSpells())
            items.emplace_back(spell->getName() + " (" + std::to_string(spell->m_mana) + ")");

        Menu menu(m_main, items, 4, Config::TextIndentation);
        // choose type of attack
        int chosen = menu.init();
        // regular attack
        if (chosen == 0) {
            HeroMove();
        } else {
            std::shared_ptr <Spell> spell = m_hero->getSpells().at(chosen - 1);
            if (!m_hero->hasEnoughMana(spell->m_mana)) {
                printNotEnoughMana(spell->m_mana);
                continue;
            }
            HeroMove(*spell);
        }


        if (checkEndOfFight())
            break;

        MonsterMove();

        if (checkEndOfFight())
            break;
    }
    if (m_hero->isAlive()) {
        printVictory();
        return true;
    }
    printDefeat();
    return false;
}

bool FightEngine::checkEndOfFight() const {
    return !m_hero->isAlive() || !m_monster->isAlive();
}





