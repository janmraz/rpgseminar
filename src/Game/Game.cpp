//
// Created by Jan Mráz on 2019-04-04.
//

#include "Game.h"
#include "Hero/Mage.h"
#include "Hero/Orc.h"
#include "Hero/Warrior.h"
#include "Items/Armor.h"
#include "Items/Weapon.h"
#include "Items/Potion.h"
#include "FightEngine.h"
#include "Monsters/Dragon.h"
#include "Menu.h"
#include "Items/Elixir.h"
#include "Monsters/Werewolf.h"
#include "Monsters/Skeleton.h"
#include "Config.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ncurses.h>
#include <zconf.h>


Game::Game() {
    // init random generator
    srand((unsigned) time(0));

    int parent_x, parent_y;
    int score_size = 5;
    int inventory_size = 50;

    initscr();
    noecho();
    curs_set(FALSE);

    // set up initial windows
    getmaxyx(stdscr, parent_y, parent_x);

    // set up window sizes
    m_main = newwin(parent_y - score_size, parent_x - inventory_size, 0, 0);
    m_bottom = newwin(score_size, parent_x - inventory_size, parent_y - score_size, 0);
    m_side = newwin(parent_y, parent_x - inventory_size, 0, parent_x - inventory_size);
}

// -----------------------------------------------------------------------------------------------------------
// Loading functions
// -----------------------------------------------------------------------------------------------------------


void Game::loadMonster(std::ifstream &file, char monsterType) {
    std::string name, delimiter;
    int hp, armor, attack, x, y;

    std::getline(file, name, ';');
    file >> hp;
    std::getline(file, delimiter, ';');
    file >> armor;
    std::getline(file, delimiter, ';');
    file >> attack;
    std::getline(file, delimiter, ';');
    file >> x;
    std::getline(file, delimiter, ';');
    file >> y;

    std::cout << name << " " << hp << " " << armor << " " << attack << " " << x << " " << y << std::endl;

    switch (monsterType) {
        case Werewolf::shortcut:
            m_monsters.push_back(std::make_shared<Werewolf>(name, hp, armor, attack, x, y));
            break;
        case Skeleton::shortcut:
            m_monsters.push_back(std::make_shared<Skeleton>(name, hp, armor, attack, x, y));
            break;
        case Dragon::shortcut:
            m_monsters.push_back(std::make_shared<Dragon>(name, hp, armor, attack, x, y));
            break;
        default:
            throw InvalidMonsterType();
    }
    m_map.addMonster(m_monsters.back(), x, y);

    std::getline(file, delimiter, '\n');
    if (!delimiter.empty())
        throw LineHasTooManyColumns();
}

void Game::loadItem(std::ifstream &file, char itemType, bool addToInventory = false) {
    std::string name, description, delimiter;
    int amount, x = 0, y = 0;

    std::getline(file, name, ';');
    std::getline(file, description, ';');
    file >> amount;
    if (!addToInventory) {
        std::getline(file, delimiter, ';');
        file >> x;
        std::getline(file, delimiter, ';');
        file >> y;
    }

    switch (itemType) {
        case Armor::shortcut:
            m_items.push_back(std::make_shared<Armor>(name, description, amount, x, y));
            break;
        case Weapon::shortcut:
            m_items.push_back(std::make_shared<Weapon>(name, description, amount, x, y));
            break;
        case Potion::shortcut:
            m_items.push_back(std::make_shared<Potion>(name, description, amount, x, y));
            break;
        case Elixir::shortcut:
            m_items.push_back(std::make_shared<Elixir>(name, description, amount, x, y));
            break;
        default:
            throw InvalidItemType();
    }
    if (addToInventory) {
        m_items.back()->loot();
        m_player->addItem(m_items.back());
    } else {
        m_map.addItem(m_items.back(), x, y);
    }

    std::getline(file, delimiter, '\n');
    if (!delimiter.empty())
        throw LineHasTooManyColumns();
}

void Game::loadMonsters(const std::string &path = "monsters.txt") {
    std::ifstream file(path);
    std::string cell;
    while (true) {
        if (!std::getline(file, cell, ';'))
            break;
        if (cell.size() > 1)
            throw InvalidMonsterType();
        std::cout << "loading monster of type " << cell[0] << std::endl;
        loadMonster(file, cell[0]);
    }
    std::cout << "done loading monsters" << std::endl;
}

void Game::loadItems(const std::string &path = "items.txt") {
    std::ifstream file(path);
    std::string cell;
    while (true) {
        if (!std::getline(file, cell, ';'))
            break;
        if (cell.size() > 1)
            throw InvalidItemType();
        std::cout << "loading item of type " << cell[0] << std::endl;
        loadItem(file, cell[0]);
    }
    std::cout << "done loading items" << std::endl;
}

void Game::loadInventoryItems(std::ifstream &file) {
    std::string cell;
    while (true) {
        if (!std::getline(file, cell, ';'))
            break;
        if (cell.size() > 1) {
            std::cout << "ok" << cell << std::endl;
            throw InvalidItemType();
        }
        std::cout << "loading inventory item of type " << cell[0] << std::endl;
        loadItem(file, cell[0], true);
    }
    std::cout << "done loading inventory items" << std::endl;
}

void Game::loadUser(const std::string &path) {
    std::ifstream file(path);
    std::string cell;
    std::string name, delimiter;
    int hp, armor, attack, mana, x, y;

    std::cout << path << std::endl;

    if (!std::getline(file, cell, ';') || cell.size() > 1)
        throw InvalidUserType();

    std::getline(file, name, ';');
    file >> hp;
    std::getline(file, delimiter, ';');
    file >> attack;
    std::getline(file, delimiter, ';');
    file >> armor;
    std::getline(file, delimiter, ';');
    file >> mana;
    std::getline(file, delimiter, ';');
    file >> x;
    std::getline(file, delimiter, ';');
    file >> y;

    std::getline(file, delimiter, '\n');
    if (!delimiter.empty())
        throw LineHasTooManyColumns();

    std::cout << name << " " << hp << " " << armor << " " << attack << " " << mana << " " << x << " " << y << std::endl;

    switch (cell[0]) {
        case Mage::shortcut:
            m_player = std::make_shared<Mage>(name, hp, armor, attack, mana, x, y);
            break;
        case Warrior::shortcut:
            m_player = std::make_shared<Warrior>(name, hp, armor, attack, mana, x, y);
            break;
        case Orc::shortcut:
            m_player = std::make_shared<Orc>(name, hp, armor, attack, mana, x, y);
            break;
        default:
            throw InvalidUserType();
    }

    loadInventoryItems(file);
}

void Game::loadDefaultGame() {
    m_map = Map("default/map.txt", m_player);
    loadItems("default/items.txt");
    loadMonsters("default/monsters.txt");
}


bool Game::checkFileExistence(const std::string &path) {
    std::fstream fileStream;
    fileStream.open(path);
    return !fileStream.fail();
}

void Game::loadGame() {
    std::string name = getTextInput("Enter name of saved game: ");

    if (!checkFileExistence("saved/" + name + ".user.txt") || !checkFileExistence("saved/" + name + ".map.txt") ||
        !checkFileExistence("saved/" + name + ".monsters.txt") || !checkFileExistence("saved/" + name + ".items.txt"))
        return mainMenu();

    m_items.clear();
    m_monsters.clear();

    loadUser("saved/" + name + ".user.txt");
    m_map = Map("saved/" + name + ".map.txt", m_player);
    loadItems("saved/" + name + ".items.txt");
    loadMonsters("saved/" + name + ".monsters.txt");

    control();
}

// -----------------------------------------------------------------------------------------------------------
// Main scenes
// -----------------------------------------------------------------------------------------------------------

void Game::playerChoosing() {
    wclear(m_main);

    mvwprintw(m_main, 1, Config::TextIndentation, "Now you will choose your Hero");
    mvwprintw(m_main, 3, Config::TextIndentation, "What Hero would you like to play?");

    std::vector <std::string> items;
    items.emplace_back("Mage");
    items.emplace_back("Orc");
    items.emplace_back("Warrior");
    items.emplace_back("Back");
    Menu menu(m_main, items, 5, Config::TextIndentation);
    int c = menu.init();

    std::string header = "Enter your hero\'s name: ";
    switch (c) {
        case 0:
            m_player = std::make_shared<Mage>(getTextInput(header), 1, 1);
            break;
        case 1:
            m_player = std::make_shared<Orc>(getTextInput(header), 1, 1);
            break;
        case 2:
            m_player = std::make_shared<Warrior>(getTextInput(header), 1, 1);
            break;
        default:
            start();
            return;
    }
    loadDefaultGame();
    control();
}

void Game::mainMenu() {
    // enable input inside window
    keypad(m_main, true);
    wclear(m_main);
    wclear(m_side);

    mvwprintw(m_main, 1, Config::TextIndentation, "Welcome to the Forbidden Dungeon");
    mvwprintw(m_main, 3, Config::TextIndentation,
              "Forbidden Dungeon is RPG game situated in dark dungeons filled with monsters and ancient items");
    mvwprintw(m_main, 5, Config::TextIndentation, "Are you ready to enter the Forbidden Dungeon ?");
    mvwprintw(m_main, 8, Config::TextIndentation, "Main menu:");

    refreshScreen();

    std::vector <std::string> items;
    items.emplace_back("New Game");
    items.emplace_back("Load Game");
    items.emplace_back("Exit");
    Menu menu(m_main, items, 9, Config::TextIndentation);
    int c = menu.init();
    switch (c) {
        case 0:
            playerChoosing();
            break;
        case 1:
            loadGame();
            break;
        default:
            endwin();
            break;
    }
}

void Game::resumeMenu() {
    // clear window
    wclear(m_main);
    // enable input inside window
    keypad(m_main, true);

    mvwprintw(m_main, 1, Config::TextIndentation, "Resumed game");
    mvwprintw(m_main, 3, Config::TextIndentation, "Menu:");

    refreshScreen();

    std::vector <std::string> items;
    items.emplace_back("Return back");
    items.emplace_back("Save Game");
    items.emplace_back("Exit Game");
    Menu menu(m_main, items, 5, Config::TextIndentation);
    int c = menu.init();
    switch (c) {
        case 0:
            break;
        case 1:
            saveGame();
            break;
        case 2:
            mainMenu();
            break;
        default:
            break;
    }
}

void Game::control() {
    bool ended = false;
    while (true) {
        wclear(m_main);
        printMap();

        mvwprintw(m_bottom, 1, 1, "You can navigate thru game with arrows up, down and left."
                                  "Press m to enter menu");

        refreshStats(m_side, m_player);
        refreshScreen();

        int ch = wgetch(m_main);
        mvwprintw(m_bottom, 3, 1, "%c", ch);
        switch (ch) {
            case KEY_UP:
                m_map.goAhead();
                if (m_map.isEnemy() && fight(m_map.getEnemy()))
                    ended = true;
                break;
            case KEY_RIGHT:
                m_map.rotatePlayer();
                break;
            case KEY_LEFT:
                m_map.rotatePlayer(false);
                break;
            case 'm':
                resumeMenu();
                break;
            default:
                break;
        }
        if (ended)
            break;
    }
    mainMenu();
}

std::string Game::getTextInput(const std::string &header) {
    wclear(m_main);
    echo();
    char str[Config::TextLimit];

    refreshScreen();

    mvprintw(3, Config::TextIndentation, "%s", header.c_str());
    getnstr(str, Config::TextLimit);

    noecho();
    std::string in = str;
    return in;
}

bool Game::fight(std::shared_ptr <Monster> &monster) {
    FightEngine fe(monster, m_player, m_main, m_side);

    if (!fe.start()) {
        printDefeat();
        return true;
    }

    m_map.removeEnemy();
    if (checkTotalVictory()) {
        printVictory();
        return true;
    }

    return false;
}


void Game::saveGame() {
    std::string name = getTextInput("Enter name of saved game: ");

    std::ofstream user("saved/" + name + ".user.txt");
    user << m_player->getCharShortcut() << ";" << m_player->getName() << ";" << m_player->getHP() << ";"
         << m_player->getAttack() << ";" << m_player->getArmor() << ";" << m_player->getMana() << ";"
         << m_player->getPositionX()
         << ";" << m_player->getPositionY() << std::endl;

    for (const auto &item : m_player->getItems())
        user << item->getCharShortcut() << ";" << item->m_name << ";" << item->m_description << ";"
             << item->getAmount() << std::endl;

    std::ofstream items("saved/" + name + ".items.txt");
    for (const auto &item : m_items)
        if (!item->wasLooted())
            items << item->getCharShortcut() << ";" << item->m_name << ";" << item->m_description << ";"
                  << item->getAmount() << ";" << item->getPositionX() << ";" << item->getPositionY() << std::endl;

    std::ofstream monsters("saved/" + name + ".monsters.txt");
    for (const auto &monster : m_monsters)
        if (monster->isAlive())
            monsters << monster->getCharShortcut() << ";" << monster->getName() << ";" << monster->getHP() << ";"
                     << monster->getAttack() << ";" << monster->getArmor() << ";" << monster->getPositionX() << ";"
                     << monster->getPositionY() << std::endl;

    std::ofstream map("saved/" + name + ".map.txt");
    m_map.exportMap(map);
}

// -----------------------------------------------------------------------------------------------------------
// Start function and other helper functions
// -----------------------------------------------------------------------------------------------------------

void Game::start() {
    mainMenu();
}

void Game::printMap() {
    m_map.printMap(m_main);
}

bool Game::checkTotalVictory() {
    for (const auto &moster : m_monsters)
        if ((*moster).isAlive())
            return false;
    return true;
}

// -----------------------------------------------------------------------------------------------------------
// Ending scenes
// -----------------------------------------------------------------------------------------------------------


void Game::printVictory() {
    drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "Congratulations !!!");
    mvwprintw(m_main, 6, Config::TextIndentation, "You have won.");
    mvwprintw(m_main, 8, Config::TextIndentation, "Whole Dungeion is clear due to your bravery.");
    mvwprintw(m_main, 10, Config::TextIndentation, "< press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

void Game::printDefeat() {
    drawBorders(m_main);

    mvwprintw(m_main, 4, Config::TextIndentation, "You have lost!");
    mvwprintw(m_main, 6, Config::TextIndentation, "Unfortunetely you have died.");
    mvwprintw(m_main, 8, Config::TextIndentation, "Try next time");
    mvwprintw(m_main, 10, Config::TextIndentation, "< press -> to continue >");

    while (wgetch(m_main) != KEY_RIGHT);
}

// -----------------------------------------------------------------------------------------------------------
// Static helper functions
// -----------------------------------------------------------------------------------------------------------

void Game::drawBorders(WINDOW *window) {
    int x, y, i;

    getmaxyx(window, y, x);

    // 4 corners
    mvwprintw(window, 0, 0, "+");
    mvwprintw(window, y - 1, 0, "+");
    mvwprintw(window, 0, x - 1, "+");
    mvwprintw(window, y - 1, x - 1, "+");

    // sides
    for (i = 1; i < (y - 1); i++) {
        mvwprintw(window, i, 0, "|");
        mvwprintw(window, i, x - 1, "|");
    }

    // top and bottom
    for (i = 1; i < (x - 1); i++) {
        mvwprintw(window, 0, i, "-");
        mvwprintw(window, y - 1, i, "-");
    }
}

void Game::refreshScreen() {
    drawBorders(m_main);
    drawBorders(m_side);
    drawBorders(m_bottom);

    wrefresh(m_main);
    wrefresh(m_bottom);
    wrefresh(m_side);
}

void Game::refreshStats(WINDOW *window, const std::shared_ptr <Hero> &player) {
    wclear(window);
    mvwprintw(window, 1, 1, "Hero\'s name: %s", player->getName().c_str());
    mvwprintw(window, 3, 1, "Hp: %d", player->getHP());
    mvwprintw(window, 4, 1, "Mana: %d", player->getMana());
    mvwprintw(window, 5, 1, "Armor: %d", player->getArmor());
    mvwprintw(window, 6, 1, "Attack: %d", player->getAttack());
    mvwprintw(window, 8, 1, "Your Inventory:");

    int i = 9;
    for (auto &c : player->getItems())
        mvwprintw(window, i++, 3, "%s", (*c).m_name.c_str());

    drawBorders(window);
    wrefresh(window);
}
