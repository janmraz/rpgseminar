//
// Created by Jan Mráz on 2019-04-05.
//

#ifndef RPG_SEMINAR_CHARACTER_H
#define RPG_SEMINAR_CHARACTER_H


#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <memory>
#include "Spells/Spell.h"
#include "Items/Item.h"
#include "Entity.h"

/**
 * Exception thrown if there was an attempt to initialize character with invalid stats
 */
class InvalidCharacterStats : std::exception {
};

/**
  * Character represents abstract class which is responsible for basic functionality for heroes and monsters.
  * It is subclass of {@link Entity} so it has position and char representation.
  * It unites functions of subclasses {@link Hero} and {@link Monster} such as decreasing HP or mana,
  * storing hp, mana, attack, armor amount, storing name, adding items into inventory.
  */
class Character : public Entity {
protected:
    std::string m_name;
    int m_hp;
    int m_armor;
    int m_attack;
    int m_mana;
    std::vector<std::shared_ptr<Spell>> m_spells;
    std::vector<std::shared_ptr<Item>> m_inventory;

    Character(const std::string &name, int hp, int armor, int attack, int mana, int x, int y);

public:
    /**
     * @inheritdoc
     */
    char getCharShortcut() const override = 0;

    /**
     * @inheritdoc
     */
    void setPositionX(int x) override = 0;

    /**
    * @inheritdoc
    */
    void setPositionY(int y) override = 0;

    /**
     * Gets current mana of an character
     *
     * @return character's mana amount
     */
    virtual int getMana() const = 0;

    /**
     * Decrease certain amount of mana
     *
     * @param mana amount
     */
    virtual void useMana(int mana) = 0;

    /**
     * Gets attack power of an character
     *
     * @return int representing attacking power
     */
    int getAttack() const;

    /**
     * Returns character's {@link Spell spells}
     *
     * @returns vector of spells
     */
    const std::vector<std::shared_ptr<Spell>> &getSpells() const;

    /**
     * Decrease certain amount of HP
     *
     * @param hp how much hp will decreased
     */
    void getHit(int hp);

    /**
     * Gets current HP of an character
     *
     * @return int representing HP amount
     */
    int getHP() const;

    /**
     * Gets armor of an character
     *
     * @return int representing armor amount
     */
    int getArmor() const;

    /**
     * Checks whether character is alive
     *
     * @return true, if character is alive
     */
    bool isAlive() const;

    /**
     * Adds {@link Item item} into inventory
     *
     * @param item shared_ptr to item that is being added
     */
    void addItem(const std::shared_ptr<Item> &item);

    /**
     * Returns character's entire inventory as vector of {@link Item items}
     *
     * @returns vector of items
     */
    std::vector<std::shared_ptr<Item>> &getItems();

    /**
     * Returns character's name
     *
     * @returns name of character
     */
    const std::string &getName() const;

    /**
     * Checks whether character has enough mana to perform spell for which it needs provided mana amount
     *
     * @param mana required
     * @return true, if character has enough mana
     */
    virtual bool hasEnoughMana(int mana) const = 0;


};


#endif //RPG_SEMINAR_CHARACTER_H
