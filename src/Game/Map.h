//
// Created by Jan Mráz on 2019-04-04.
//

#ifndef RPG_SEMINAR_MAP_H
#define RPG_SEMINAR_MAP_H


#include "Place.h"
#include "Hero/Hero.h"
#include <vector>

/**
 * Exception thrown if there was an attempt to access place out of map.
 */
class OutOfMapBoundaries : std::exception {
};

/**
 * Exception thrown if there was an attempt to save monster into wall.
 */
class AddingMonsterIntoWall : std::exception {
};

/**
 * Exception thrown if there was an attempt to save item into wall.
 */
class AddingItemIntoWall : std::exception {
};

/**
 * Exception thrown if there was an attempt to save two or more monsters on one place.
 */
class MoreMonstersInOnePlace : std::exception {
};

/**
 * Exception thrown if member variable orientation is in invalid state(different then >, <, ^, V).
 */
class InvalidOrientation : std::exception {
};

/**
 * Exception thrown if player's initial position is in in the wall.
 */
class PlayerPlacedInWall : std::exception {
};

/**
 * Exception thrown if someone is trying to get monster which is not there
 */
class NoMonsterInPlace : std::exception {
};

/**
 * Map is main class responsible for dealing with map. It loads map from path or save it to file.
 * It is also responsible for moving and rotating player, looting chests or detecting enemy.
 */
class Map {
    std::vector<std::vector<Place>> m_map;
    int m_height;
    int m_width;
    PLACE_TYPE m_orientation = UserLeft;

    std::shared_ptr<Hero> m_player;

    /**
     * Updates place type on player's position to corresponding orientation
     */
    void updatePlayer();

    /**
     * Gets Place object ahead of player's orientation
     *
     * @return Place ahead
     */
    Place &getAheadPlace();

    /**
     * Checks whether provided position is within map
     *
     * @param x position in map
     * @param y position in map
     * @return true if it is within boundaries
     */
    bool checkWithinBoundaries(int x, int y) const;

public:
    Map();

    /**
     * Constructs the map based on provided path and sets the player on the map
     */
    Map(const std::string &path, std::shared_ptr<Hero> &player);

    /**
     * Adds item into map on certain position
     *
     * @param item shared_ptr on item
     * @param x position in map
     * @param y position in map
     * @throw exception {@link OutOfMapBoundaries} exception if item is positioned out of map
     * @throw exception {@link AddingItemIntoWall} exception if item is added to wall
     */
    void addItem(const std::shared_ptr<Item> &item, int x, int y);

    /**
     * Adds monster into map on certain position
     *
     * @param monster shared_ptr on monster
     * @param x position in map
     * @param y position in map
     * @throw exception {@link OutOfMapBoundaries} exception if monster is positioned out of map
     * @throw exception {@link AddingMonsterIntoWall} exception if monster is added to wall
     * @throw exception {@link MoreMonstersInOnePlace} exception if two monsters are on same place
     */
    void addMonster(const std::shared_ptr<Monster> &monster, int x, int y);

    /**
     * Updates player's orientation
     *
     * @param clockWise should rotate clockwise
     * @throw exception  {@link InvalidOrientation} if invalid orientation is saved in map object. Map has inconsistent state.
     */
    void rotatePlayer(bool clockWise = true);

    /**
     * Moves player ahead and deals with consequences.It take care of chests looting and walls.
     *
     * @return true if player has moved, otherwise false.
     */
    bool goAhead();

    /**
     * Checks if on current player's position there is monster
     *
     * @returns true if there is a monster in this place
     */
    bool isEnemy() const;

    /**
     * Returns pointer on monster that is on current player's position
     *
     * @return pointer on monster
     */
    std::shared_ptr<Monster> &getEnemy();

    /**
     * Removes monster that is on current player's position
     */
    void removeEnemy();

    /**
     * Prints whole map into provided window
     *
     * @param w window where will be map printed
     */
    void printMap(WINDOW *w) const;

    /**
     * It saves map into file via provided ofstream
     *
     * @param ofstream
     */
    void exportMap(std::ofstream &ofstream) const;
};


#endif //RPG_SEMINAR_MAP_H
