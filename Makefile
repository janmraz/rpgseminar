MAKEFLAGS += -r
CXX=g++
CXXFLAGS=-Wall -pedantic -g -std=c++14 -Wno-long-long -O0 -I ./src/Game
LDLIBS=-lncurses
LOGIN=mrazjan3
SRC_DIR=./src/Game
HEADERS_DIR=./src/Game
SRCS=$(wildcard $(SRC_DIR)/*.cpp | $(SRC_DIR)/*/*.cpp )
OBJS=$(SRCS:%.cpp=./%.o)

all: compile doc

compile:  $(OBJS)
	$(CXX) $(CXXFLAGS) src/main.cpp $(OBJS) $(LDLIBS) -o $(LOGIN)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

doc:
	doxygen Doxyfile

run:
	./$(LOGIN)

clean:
	$(RM) -r $(SRC_DIR)/*.o ./doc ./$(LOGIN) && $(RM) -r $(SRC_DIR)/*/*.o

